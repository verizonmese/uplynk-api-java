package com.findmory;

/**
 * Created by mory on 1/29/16.
 */


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;
import java.util.zip.Deflater;

import com.google.gson.*;

import okhttp3.*;


public class Hash {

    public static void main(String[] args) {

        Hash main = new Hash();
        Map<String, Object> params = new HashMap<>();

        //example 1: get info on an asset by ID
        params.put("id", "998dbb0715684334a9234cd640840b43");
        main.call("/api2/asset/get", params);

        //example 2: get asset list with search params
        params.clear();
        params.put("search", "KCEN");
        params.put("order", "-created");
        main.call("/api2/asset/list", params);
    }

    public String ROOT_URL = "http://services.uplynk.com";
    public String OWNER = "";// # CHANGE THIS TO YOUR ACCOUNT GUID
    public String SECRET = "";// # CHANGE THIS TO YOUR SECRET API KEY


    public void call(String url, Map<String, Object> messageParameters) {
        messageParameters.put("_owner", OWNER);
        messageParameters.put("_timestamp", (new Date().getTime() / 1000));

        try {

            Gson gson = new Gson();
            String json = gson.toJson(messageParameters);
            System.out.println("json: " + json);

            String msg = zlib9(json);
            System.out.println("zlib + b64: " + msg);

            String sig = calcSignature(msg, SECRET);
            System.out.println("sig: " + sig);


            OkHttpClient client = new OkHttpClient();
            //build the form-urlencoded body to pass to HTTP request
            //TODO: add any other params
            RequestBody formBody = new FormBody.Builder()
                    .add("msg", msg)
                    .add("sig", sig)
                    .build();

            Request request = new Request.Builder()
                    .url(ROOT_URL + url)
                    .post(formBody)
                    .addHeader("cache-control", "no-cache")
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .build();

            Response response = client.newCall(request).execute();
            System.out.println("response body: " + response.body().string());


        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException | IOException e){
            e.printStackTrace();
        }


    }

    public static String zlib9(String data) throws IOException {
        byte[] dataBytes = data.getBytes();
        Deflater deflater = new Deflater();
        deflater.setLevel(Deflater.BEST_COMPRESSION);
        deflater.setInput(dataBytes);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(dataBytes.length);
        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] ba = outputStream.toByteArray();

        String encoded = Base64.getEncoder().encodeToString(ba);
        return encoded;
    }


    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA256";

    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();

        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return formatter.toString();
    }

    public static String calcSignature(String data, String key)
            throws SignatureException, NoSuchAlgorithmException, InvalidKeyException
    {
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
        mac.init(signingKey);
        return toHexString(mac.doFinal(data.getBytes()));
    }


}
